from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager
from rest_framework_api_key.models import AbstractAPIKey


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        unique=True, blank=False, null=False,
    )
    date_joined = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)
    email_validated = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    objects = UserManager()

    class Meta:
        db_table = 'users'


class UserAPIKey(AbstractAPIKey):
    ACCOUNT_ACTIVATION = 'Account Activation'
    PASSWORD_RESET = 'Password Reset'

    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='api_keys'
    )

    class Meta(AbstractAPIKey.Meta):
        db_table = 'user_api_keys'

