import logging
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import PermissionDenied
from rest_framework_api_key.permissions import BaseHasAPIKey
from .models import UserAPIKey

logger = logging.getLogger('core')


class HasUserAPIKey(BaseHasAPIKey):
    model = UserAPIKey

    def has_permission(self, request, view):
        result = super().has_permission(request, view)
        if not result:
            error_message = 'Sorry, it seems the link has expired!'
            logger.error(error_message)
            raise PermissionDenied(_(error_message))
        return result
