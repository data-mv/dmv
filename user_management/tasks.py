from django.conf import settings
from celery import shared_task
from utils.email import send_mail


@shared_task
def send_activation_link(email, activation_key):
    subject = "Welcome to DMV"
    body = f"Please click on the link to activate your account " \
           f"{settings.WEB_CLIENT_ACTIVATE_URL + activation_key}"
    html_content = f"<h4>Please click the link to validate your email</h4> " \
                   f"<p>{settings.WEB_CLIENT_ACTIVATE_URL}{activation_key}</p>"
    send_mail(subject, body, [email], html_content=html_content)


@shared_task
def send_password_reset_link(email, password_api_key):
    subject = "Password Reset Link"
    body = f"Please click on the link to reset your password " \
           f"{settings.WEB_CLIENT_RESET_URL + password_api_key}"
    html_content = f"<h4>Please click the link below to reset your password</h4> " \
                   f"<p>{settings.WEB_CLIENT_RESET_URL}{password_api_key}</p>"
    send_mail(subject, body, [email], html_content=html_content)
