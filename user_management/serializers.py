from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.exceptions import AuthenticationFailed
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer as _TokenObtainPairSerializer

from utils.exceptions import AccountDeactivated, UnvalidatedAccount
from .models import User


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        validators=[
            UniqueValidator(
                User.objects.all(),
                lookup='iexact',
                message='Email has been taken already'
            )
        ],
        max_length=254
    )

    class Meta:
        model = User
        fields = ['email', 'password']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User(email=validated_data['email'])
        user.set_password(validated_data['password'])
        user.save()
        return user


class PasswordUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['password']
        extra_kwargs = {'password': {'write_only': True}}


class TokenObtainPairSerializer(_TokenObtainPairSerializer):

    def validate(self, attrs):
        data = super().validate(attrs)

        if self.user.is_deleted:
            raise AccountDeactivated

        if not self.user.email_validated:
            raise UnvalidatedAccount
        return data
