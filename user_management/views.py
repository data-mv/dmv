import logging
from datetime import datetime, timedelta
from rest_framework.generics import CreateAPIView, RetrieveAPIView, \
    UpdateAPIView
from rest_framework.response import Response
from rest_framework.exceptions import NotFound, ParseError
from rest_framework_api_key.permissions import KeyParser
from rest_framework_simplejwt.views import \
    TokenObtainPairView as _TokenObtainView, TokenRefreshView as _TokenRefreshView
from .serializers import UserSerializer, PasswordUpdateSerializer, TokenObtainPairSerializer
from .models import User, UserAPIKey
from .permissions import HasUserAPIKey
from utils import check_if_account_is_active, check_if_account_is_valid
from utils.custom_response import CustomResponse
from user_management.tasks import send_activation_link, send_password_reset_link


logger = logging.getLogger('core')


class UserView(CreateAPIView):
    """
    Concrete view for creating user resource
    """
    serializer_class = UserSerializer
    authentication_classes = []
    queryset = User.objects.all()
    error_message = 'Registration Failed'

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, *kwargs)
        return CustomResponse(
            response,
            message='User was successfully registered'
        ).success()

    def perform_create(self, serializer):
        email = serializer.validated_data['email']
        user_instance = serializer.save()
        _, key = UserAPIKey.objects.create_key(
            expiry_date=datetime.max,
            name=UserAPIKey.ACCOUNT_ACTIVATION,
            user=user_instance
        )
        send_activation_link.delay(email, key)


class TokenObtainPairView(_TokenObtainView):
    error_message = 'Authentication Failed'
    serializer_class = TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        return CustomResponse(
            response,
            message='Login was successful'
        ).success()


class TokenRefreshView(_TokenRefreshView):
    error_message = 'Authentication Failed'

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        return CustomResponse(
            response,
            message='Token refresh was successful'
        ).success()


class PasswordResetLinkView(RetrieveAPIView):
    authentication_classes = []
    error_message = 'Password Reset Failed'
    queryset = User.objects.all()

    def get_object(self):
        email = self.request.query_params.get('email')
        if not email:
            logger.error('Either the email key is missing from '
                         'the query_params or it\'s value is empty')
            raise ParseError('Email was not sent')
        user = User.objects.filter(email=email).first()

        check_if_account_is_active(user)
        check_if_account_is_valid(user)
        return user

    def _invalidate_existing_keys(self, email):
        UserAPIKey.objects.filter(name=email).update(revoked=True)

    def get(self, request, *args, **kwargs):
        user = self.get_object()
        if user:
            self._invalidate_existing_keys(user.email)
            expiration_date = datetime.now() + timedelta(minutes=60)
            _, key = UserAPIKey.objects.create_key(
                name=UserAPIKey.PASSWORD_RESET,
                expiry_date=expiration_date,
                user=user
            )
            send_password_reset_link.delay(user.email, key)
        message = 'A reset link has been sent to your email'
        logger.info(message)
        return CustomResponse(Response(), message=message).success()


class VerifyUserApiKeyView(RetrieveAPIView):
    permission_classes = [HasUserAPIKey]
    authentication_classes = []
    error_message = 'ApiKey verification failed'
    queryset = UserAPIKey.objects.all()

    def get_object(self):
        api_key = KeyParser().get(self.request)
        return UserAPIKey.objects.get_from_key(api_key)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        user = instance.user
        check_if_account_is_active(user)

        if instance.name == UserAPIKey.ACCOUNT_ACTIVATION:
            user.email_validated = True
            user.save()
            instance.revoked = True
            instance.save()
            message = 'Your Account has been validated'
        else:
            check_if_account_is_valid(user)
            message = 'APIKey verification was successful'
        logger.info(message)
        return CustomResponse(Response(), message=message).success()


class PasswordUpdateView(UpdateAPIView):
    permission_classes = [HasUserAPIKey]
    authentication_classes = []
    error_message = 'Password update operation failed'
    queryset = User.objects.all()
    serializer_class = PasswordUpdateSerializer
    api_key = None

    def get_object(self):
        api_key = KeyParser().get(self.request)
        instance = UserAPIKey.objects.get_from_key(api_key)
        self.api_key = instance
        return instance.user

    def perform_update(self, serializer):
        instance = serializer.instance
        instance.set_password(serializer.validated_data.get('password'))
        instance.save()
        self.api_key.revoked = True
        self.api_key.save()

    def update(self, request, *args, **kwargs):
        super().update(request, *args, **kwargs)
        message = 'Password update was successful'
        return CustomResponse(Response(), message=message).success()


