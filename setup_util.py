import os
import environ


def configure_environment():
    env = environ.Env()
    environment = env('DJANGO_ENV')
    possible_env_values = {'development', 'production', 'test'}
    if environment.lower() not in possible_env_values:
        raise ValueError(f"DJANGO_ENV value should only be any of these values {' '.join(possible_env_values)}")

    if environment != 'production':
        root_dir = os.path.dirname(os.path.abspath(__file__))
        if not os.path.exists(f"{root_dir}/.env"):
            raise FileExistsError("env file does not exist. Please create one")
        env.read_env(f"{root_dir}/.env")

    os.environ.setdefault(
        'DJANGO_SETTINGS_MODULE',
        f'core.settings.{environment}'
    )
