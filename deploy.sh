#!/bin/bash

pip install -r requirements.txt
python manage.py makemigrations

# skip migrate for now until this is clear
python manage.py migrate
