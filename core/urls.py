"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path, include
from user_management.views import (
    PasswordResetLinkView,
    TokenObtainPairView,
    TokenRefreshView,
    VerifyUserApiKeyView,
    PasswordUpdateView
)
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/users/', include('user_management.urls')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/password-reset/', PasswordResetLinkView.as_view(), name='password_reset'),
    path('api/verify-apikey/', VerifyUserApiKeyView.as_view(), name='verify_api_key'),
    path('api/password-update/', PasswordUpdateView.as_view(), name='password_update')
]
