from setup_util import configure_environment


def main():
    from celery import Celery
    configure_environment()

    app = Celery('core')
    app.config_from_object('django.conf:settings', namespace='CELERY')
    app.autodiscover_tasks()
    return app


celery_app = main()
