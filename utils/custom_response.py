from rest_framework.response import Response


class CustomResponse:
    def __init__(self, response, message=None):
        assert isinstance(response, (Response,)), (
            'response should be an instance of Response'
        )
        self._response = response
        self._status_code = response.status_code
        self._data = response.data
        self._message = message

    @staticmethod
    def _format_error(error):
        formatted_error = []
        if not error:
            return formatted_error
        for key, value in error.items():
            if type(value) is list:
                formatted_error.extend(value)
            else:
                formatted_error.append(value)
        return formatted_error

    def success(self):
        self._response.data = {
            'status': 'success',
            'status_code': self._status_code,
            'message': self._message or 'Operation was successful',
            'data': self._data
        }
        return self._response

    def error(self):
        self._response.data = {
            'status': 'failed',
            'status_code': self._status_code,
            'message': self._message,
            'error': self._data,
            'formatted_error': self._format_error(self._data)
        }
        return self._response
