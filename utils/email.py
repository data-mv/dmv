from django.core.mail import EmailMultiAlternatives
from django.conf import settings


def compose_mail(subject, body, to, cc=(), bcc=(), html_content=None):
    assert_error_msg = 'should be a list or a tuple'
    assert isinstance(to, (list, tuple)), f"to {assert_error_msg}"
    assert isinstance(cc, (list, tuple)), f"cc {assert_error_msg}"
    assert isinstance(bcc, (list, tuple)), f"bcc {assert_error_msg}"

    email = EmailMultiAlternatives(
        subject=str(subject),
        body=str(body),
        from_email=settings.SENDER_EMAIL,
        to=map(lambda value: value.lower(), to),
        cc=map(lambda value: value.lower(), cc),
        bcc=map(lambda value: value.lower(), bcc)
    )
    if html_content:
        email.attach_alternative(html_content, 'text/html')
    return email


def send_mail(subject, body, to, html_content=None):
    compose_mail(subject, body, to, html_content=html_content).send()


def send_mail_silently(subject, body, to, html_content=None):
    compose_mail(
        subject, body,
        to, html_content=html_content
    ).send(fail_silently=True)
