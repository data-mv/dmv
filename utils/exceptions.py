from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import APIException
from rest_framework import status


class AccountDeactivated(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('Account has been deactivated. '
                       'Please contact your admin!')
    default_code = 'account_deactivated'


class UnvalidatedAccount(APIException):
    status_code = status.HTTP_403_FORBIDDEN
    default_detail = _('Account yet to be validated. '
                       'Please check email you registered with for validation link!')
    default_code = 'unvalidated_account'
