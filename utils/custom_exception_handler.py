import logging
from rest_framework.views import exception_handler
from rest_framework.response import Response
from .custom_response import CustomResponse

logger = logging.getLogger('core')


def main(exc, context):
    response = exception_handler(exc, context) or Response(
        data={'details': 'Sorry, something went wrong'},
        status=500,
        content_type='application/json'
    )
    logger.exception(exc) if response.status_code == 500 else logger.error(exc)
    return CustomResponse(
        response,
        context['view'].error_message or 'Operation Failed'
    ).error()

