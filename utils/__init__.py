from utils.exceptions import AccountDeactivated, UnvalidatedAccount


def check_if_account_is_active(user):
    if user.is_deleted:
        raise AccountDeactivated


def check_if_account_is_valid(user):
    if not user.email_validated:
        raise UnvalidatedAccount
